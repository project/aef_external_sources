External Sources
================

External Sources allows you to manage a collection of remote Drupal, storing the necessary credentials to access them. This module does nothing in itself, it is used by others modules like Externodes.
