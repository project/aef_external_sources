<?php


/**
 * Menu callback and form builder.
 */
function aef_external_sources_add_form(&$form_state, $esid = 0) {
  $form['esid'] = array('#type' => 'value', '#value' => $esid);

  if (isset($esid)) {
    $data = aef_external_sources_load($esid);
    if (isset($data)) 
    {
      drupal_set_title(t('Edit external source %name', array('%name' => $data['name'])));
    }
  }

  // If we don't have a state or db_fetch_array() returned FALSE, load defaults.
  if (!isset($data) || $data === FALSE) {
    drupal_set_title(t('Add a new external source'));
  }

  $form['#tree'] = true;
  $form['#data'] = $data;

  $form['data']['infos'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic informations'),
  );

  $form['data']['infos']['name'] = array(
    '#type'  => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $data['infos']['name'],
    '#size'  => '16',
    '#maxlength' => '254',
    '#required' => TRUE,
    '#description' => t('Enter the name for the external source.'),
  );
  $form['data']['infos']['url'] = array(
    '#title' => t('XMLRPC URL'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => $data['infos']['url'],
  );

  $form['data']['infos']['key'] = array(
    '#title' => t('XMLRPC keys'),
    '#type' => 'textarea',
    '#default_value' => $data['infos']['key'],
    '#description' => t('The XMLRPC service keys. It can be either only one key, or a list of host:keys lines. The second usecase is useful when you may call the remote drupal with multiple hosts.'),
  );

  $form['data']['infos']['login'] = array(
    '#title' => t('XMLRPC login'),
    '#type' => 'textfield',
    '#size'  => '16',
    '#maxlength' => '254',
    '#default_value' => $data['infos']['login'],
  );

  $form['data']['infos']['password'] = array(
    '#title' => t('XMLRPC password'),
    '#type' => 'password',
    '#size'  => '16',
    '#maxlength' => '254',
  );
  $form['data']['infos']['password_re'] = array(
    '#title' => t('Retype the XMLRPC password'),
    '#type' => 'password',
    '#size'  => '16',
    '#maxlength' => '254',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 10,
  );
  return $form;
}

/**
 * Validate the state addition form.
 */
function aef_external_sources_add_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if($values['data']['infos']['password'] != "" && 
    $values['data']['infos']['password_re'] != $values['data']['infos']['password'])
  {
    form_set_error('password', t('The passwords do not match.'));
  }
}

/**
 * Submit handler for state addition form.
 */
function aef_external_sources_add_form_submit($form, &$form_state) {
  aef_external_sources_set($form_state['values']['esid'], $form_state['values']['data']);

  $form_state['redirect'] = 'admin/build/external_source';
}


/**
 * Menu callback. Create the main workflow page, which gives an overview
 * of workflows and workflow states.
 */
function aef_external_sources_overview() {
  $external_sources = aef_external_sources_get_all();
  $row = array();

  foreach ($external_sources as $esid => $data) {
    $links = array(
      'workflow_overview_edit' => array(
        'title' => t('Edit'),
        'href' => "admin/build/external_source/edit/$esid"),
      'workflow_overview_delete' => array(
        'title' => t('Delete'),
        'href' => "admin/build/external_source/delete/$esid"),
    );

    $row[] = array($data['infos']['name'], theme('links', $links));

  }

  if ($row) {
    $output = theme('table', array(t('External source'), t('Operations')), $row);
  }
  else {
    $output = '<p>' . t('No external sources have been added. Would you like to <a href="@link">add an external source</a>?', array('@link' => url('admin/build/external_source/add'))) . '</p>';
  }

  return $output;
}


/**
 * Form builder. Create form for confirmation of deleting a workflow state.
 *
 */
function aef_external_sources_delete_form($form_state, $esid) {
  $data = aef_external_sources_load($esid);
  if (isset($data)) 
  {
    $form['esid'] = array(
      '#type' => 'value',
      '#value' => $esid
    );

    return confirm_form(
      $form,
      t('Are you sure you want to delete %title ?', array('%title' => $data[$esid])),
      !empty($_GET['destination']) ? $_GET['destination'] : 'admin/build/external_source',
      t('This action cannot be undone.'),
      t('Delete'),
      t('Cancel')
    );
  }
}

/**
 * Submit handler for workflow state deletion form.
 *
 * @see workflow_state_delete_form()
 */
function aef_external_sources_delete_form_submit($form, &$form_state) {
  aef_external_sources_delete($form_state['values']['esid']);
  $form_state['redirect'] = 'admin/build/external_source';
}
